**NOTICE:** 
1.在中文路径下，Build Debug Hap(s)会失败。建议将项目放置在全英文目录下。
2.此项目中library Module类型为Java Library,下载导入后需修改/library/build.gradle中的sdk依赖路径方可正常编译。
```groovy
dependencies {
    // dir改为自己本地sdk安装路径
    compileOnly fileTree(dir: 'C:/Users/Administrator/AppData/Local/Huawei/Sdk/java/2.1.1.21/api', include: ['*.jar'])
}
```

### TextDrawable
This light-weight library provides images with letter/text like the Gmail app. It extends the `ShapeElement` class thus can be used with existing/custom/network `Image` classes. Also included is a [fluent interface](http://en.wikipedia.org/wiki/Fluent_interface) for creating shapeElements and a customizable `ColorGenerator`.

<p align="center">
  <img src="https://gitee.com/openharmony-tpc/TextDrawable/raw/master/screenshot/screen0.jpg" width="35%"/>
  <img src="https://gitee.com/openharmony-tpc/TextDrawable/raw/master/screenshot/screen1.jpg" width="35%"/>
</p>

### How to use
#### install
case 1：
Generate a JAR package through the library Module and add the JAR package to the libs directory.
Add the following code in entry/gradle.build.
```gradle
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
```
case 2：
```gradle
allprojects{
    repositories{
        mavenCentral()
    }
}

implementation 'io.openharmony.tpc.thirdlib:TextDrawable:1.0.3'
```

#### 1. Create simple tile:

<p align="center">
  <img src="https://gitee.com/openharmony-tpc/TextDrawable/raw/master/screenshot/screen2.jpg"  width="350"/>
</p>

```xml
<Image ohos:width="60dp"
       ohos:height="60dp"
       ohos:id="$+id:image"/>
```
**Note:** Specify width/height for the `Image and the `drawable` will auto-scale to fit the size.
```java
TextDrawable drawable = TextDrawable.builder()
                .buildRect("A", Color.RED);
Image image = (Image) findComponentById(ResourceTable.Id_image);
image.addDrawTask(new Component.DrawTask() {
    @Override
    public void onDraw(Component component, Canvas canvas) {
    drawable.setBounds(new Rect(0, 0, image.getWidth(), image.getHeight()));
    drawable.drawToCanvas(canvas);
    }
});
```

#### 2. Create rounded corner or circular tiles:

<p align="center">
  <img src="https://gitee.com/openharmony-tpc/TextDrawable/raw/master/screenshot/screen3.jpg" width="350"/>
</p>

```java
TextDrawable drawable1 = TextDrawable.builder()
                .buildRoundRect("A", Color.RED, 10); // radius in px

TextDrawable drawable2 = TextDrawable.builder()
                .buildRound("A", Color.RED);
```

#### 3. Add border:

<p align="center">
  <img src="https://gitee.com/openharmony-tpc/TextDrawable/raw/master/screenshot/screen4.jpg" width="350"/>
</p>

```java
TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                    .withBorder(4) /* thickness in px */
                .endConfig()
                .buildRoundRect("A", Color.RED, 10);
```

#### 4. Modify font style:

```java
TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
	                .textColor(Color.BLACK)
                    .useFont(Typeface.DEFAULT)
                    .fontSize(30) /* size in px */
                    .bold()
                    .toUpperCase()
                .endConfig()
                .buildRect("a", Color.RED)
```

#### 5. Built-in color generator:

```java
ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
// generate random color
int color1 = generator.getRandomColor();
// generate color based on a key (same key returns the same color), useful for list/grid views
int color2 = generator.getColor("user@gmail.com")

// declare the builder object once.
TextDrawable.IBuilder builder = TextDrawable.builder()
				.beginConfig()
					.withBorder(4)
				.endConfig()
				.rect();

// reuse the builder specs to create multiple drawables
TextDrawable ic1 = builder.build("A", color1);
TextDrawable ic2 = builder.build("B", color2);
``` 

#### 6. Specify the width / height:

```xml
	<Image ohos:width="match_content"
		   ohos:height="match_content"
		   android:id="$+id:image"/>
```
**Note:**  The `Image` could use `match_content` width/height. You could set the width/height of the `drawable` using code.

```java
	TextDrawable drawable = TextDrawable.builder()
					.beginConfig()
						.width(60)  // width in px
						.height(60) // height in px
					.endConfig()
					.buildRect("A", Color.RED);

	Image image = (Image) findComponentById(ResourceTable.Id_image);
	image.addDrawTask(new Component.DrawTask() {
		@Override
		public void onDraw(Component component, Canvas canvas) {
		drawable.setBounds(new Rect(0, 0, image.getWidth(), image.getHeight()));
		drawable.drawToCanvas(canvas);
		}
	});
```

#### 7. Other features:

1. Mix-match with other drawables. Use it in conjunction with `StateElement`, `FrameAnimationElement` etc.

2. Compatible with other views (not just `Image`). Use it as background drawable, compound drawable for `Text`, `Button` etc.

3. Use multiple letters or `unicode` characters to create interesting tiles.

<p align="center"><img src="https://gitee.com/openharmony-tpc/TextDrawable/raw/master/screenshot/screen5.jpg" width="350"/></p>
