package com.amulyakhare.td;

import com.amulyakhare.td.sample.DataItem;
import com.amulyakhare.td.sample.DataSource;
import com.amulyakhare.textdrawable.TextDrawable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.FrameAnimationElement;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Rect;
import ohos.bundle.ElementName;
import ohos.global.resource.Resource;

public class MainAbility extends Ability implements ListContainer.ItemClickedListener {
    public static final String TYPE = "TYPE";
    private DataSource dataSource;
    private ListContainer listContainer;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);

        // super.setMainRoute(MainAbilitySlice.class.getName());
        setUIContent(ResourceTable.Layout_ability_main);
        listContainer = (ListContainer) findComponentById(ResourceTable.Id_lc);
        dataSource = new DataSource(this);
        listContainer.setItemProvider(new ListItemProvider());
        listContainer.setItemClickedListener(this);
    }

    @Override
    public void onItemClicked(ListContainer listContainer, Component component, int position, long id) {
        DataItem item = dataSource.getItem(position);

        // if navigation is supported, open the next activity
        if (item.getNavigationInfo() != DataSource.NO_NAVIGATION) {
            Intent intent = Intent.makeMainAbility(ElementName.createRelative(getBundleName(), ListAbility.class.getName(), ""));
            intent.setParam(TYPE, item.getNavigationInfo());
            startAbility(intent);
        }
    }

    private class ListItemProvider extends BaseItemProvider{

        @Override
        public int getCount() {
            return dataSource.getCount();
        }

        @Override
        public DataItem getItem(int position) {
            return dataSource.getItem(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Component getComponent(int position, Component convertView, ComponentContainer componentContainer) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = LayoutScatter.getInstance(MainAbility.this).parse(ResourceTable.Layout_layout_list_item, null, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            DataItem item = getItem(position);

            final Element drawable = item.getDrawable();

            // holder.imageView.setImageElement(drawable);由于TextDrawable中重写的drawToCanvas不会自己调用，所以这里我们主动进行调用，相当于直接在image的canvas上操作来替代setImageElement
            holder.imageView.addDrawTask(new Component.DrawTask() {
                private int index;
                private boolean isCallbackExist;
                @Override
                public void onDraw(Component component, Canvas canvas) {
                    if (drawable instanceof TextDrawable) {
                        TextDrawable td = (TextDrawable) drawable;
                        td.setBounds(new Rect(0, 0, holder.imageView.getWidth(), holder.imageView.getHeight()));
                        td.drawToCanvas(canvas);
                    } else if (drawable instanceof StateElement){
                        StateElement se = (StateElement) drawable;
                        int count = se.getStateCount();
                        for (int i = 0; i < count; i++) {
                            Element childElement = se.getStateElement(i);
                            if (childElement instanceof TextDrawable) {
                                TextDrawable td = (TextDrawable) childElement;
                                int canvasWidth = holder.imageView.getWidth();
                                int canvasHeight = holder.imageView.getHeight();
                                int perWidth = canvasWidth / count;
                                td.setBounds(new Rect(i * perWidth, 0, (i + 1) * perWidth , canvasHeight));
                                td.drawToCanvas(canvas);
                            }
                        }
                    } else if (drawable instanceof FrameAnimationElement) {
                        FrameAnimationElement fae = (FrameAnimationElement) drawable;
                        if (index >= fae.getNumberOfFrames()) {
                            index = 0;
                        }
                        TextDrawable tdInFae = (TextDrawable) fae.getFrame(index);
                        tdInFae.setBounds(new Rect(0, 0, holder.imageView.getWidth(), holder.imageView.getHeight()));
                        tdInFae.drawToCanvas(canvas);
                        index++;
                        if (!isCallbackExist) {
                            isCallbackExist = true;
                            fae.setCallback(element -> {
                                holder.imageView.invalidate();
                            });
                        }
                    } else {
                        throw new IllegalArgumentException("shouldn't go to here");
                    }
                }
            });

            holder.textView.setText(item.getLabel());

            // if navigation is supported, show the ">" navigation icon
            if (item.getNavigationInfo() != DataSource.NO_NAVIGATION) {
                Resource resource;
                try {
                    resource = MainAbility.this.getResourceManager().getResource(ResourceTable.Media_ic_action_next_item);
                } catch (Exception e) {
                    resource = null;
                }
                Element media = new PixelMapElement(resource);
                holder.textView.setAroundElements(null, null, media, null);
            }
            else {
                holder.textView.setAroundElements(null, null, null, null);
            }

            return convertView;
        }
    }

    private static class ViewHolder {

        private Image imageView;

        private Text textView;

        private ViewHolder(Component view) {
            imageView = (Image) view.findComponentById(ResourceTable.Id_imageView);
            textView = (Text) view.findComponentById(ResourceTable.Id_textView);
        }
    }
}
