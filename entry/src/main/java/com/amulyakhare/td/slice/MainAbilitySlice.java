package com.amulyakhare.td.slice;

import com.amulyakhare.td.ResourceTable;
import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Rect;

public class MainAbilitySlice extends AbilitySlice {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_abilityslice_main);
        Image image = (Image) findComponentById(ResourceTable.Id_image);
        TextDrawable td = TextDrawable.builder()
                .beginConfig()
                    .withBorder(8)
                .endConfig()
                .buildRound("H", ColorGenerator.DEFAULT.getColor("H"));
        // image.setImageElement(td);由于TextDrawable中重写的drawToCanvas不会自己调用，所以这里我们主动进行调用，相当于直接在image的canvas上操作来替代setImageElement
        image.addDrawTask(new Component.DrawTask() {
            @Override
            public void onDraw(Component component, Canvas canvas) {
                td.setBounds(new Rect(0, 0, image.getWidth(), image.getHeight()));
                td.drawToCanvas(canvas);
            }
        });

    }
}
