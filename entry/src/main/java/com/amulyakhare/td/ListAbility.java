package com.amulyakhare.td;

import com.amulyakhare.td.sample.DrawableProvider;
import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;

import java.util.Arrays;
import java.util.List;

public class ListAbility extends Ability {

    private static final int HIGHLIGHT_COLOR = 0x999be6ff;

    // list of data items
    private List<ListData> mDataList = Arrays.asList(
            new ListData("Iron Man"),
            new ListData("Captain America"),
            new ListData("James Bond"),
            new ListData("Harry Potter"),
            new ListData("Sherlock Holmes"),
            new ListData("Black Widow"),
            new ListData("Hawk Eye"),
            new ListData("Iron Man"),
            new ListData("Guava"),
            new ListData("Tomato"),
            new ListData("Pineapple"),
            new ListData("Strawberry"),
            new ListData("Watermelon"),
            new ListData("Pears"),
            new ListData("Kiwi"),
            new ListData("Plums")
    );

    // declare the color generator and drawable builder
    private ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;
    private TextDrawable.IBuilder mDrawableBuilder;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_list);
        int type = intent.getIntParam(MainAbility.TYPE, DrawableProvider.SAMPLE_RECT);
        // initialize the builder based on the "TYPE"
        switch (type) {
            case DrawableProvider.SAMPLE_RECT:
                mDrawableBuilder = TextDrawable.builder()
                        .rect();
                break;
            case DrawableProvider.SAMPLE_ROUND_RECT:
                mDrawableBuilder = TextDrawable.builder()
                        .roundRect(10);
                break;
            case DrawableProvider.SAMPLE_ROUND:
                mDrawableBuilder = TextDrawable.builder()
                        .round();
                break;
            case DrawableProvider.SAMPLE_RECT_BORDER:
                mDrawableBuilder = TextDrawable.builder()
                        .beginConfig()
                        .withBorder(4)
                        .endConfig()
                        .rect();
                break;
            case DrawableProvider.SAMPLE_ROUND_RECT_BORDER:
                mDrawableBuilder = TextDrawable.builder()
                        .beginConfig()
                        .withBorder(4)
                        .endConfig()
                        .roundRect(10);
                break;
            case DrawableProvider.SAMPLE_ROUND_BORDER:
                mDrawableBuilder = TextDrawable.builder()
                        .beginConfig()
                        .withBorder(4)
                        .endConfig()
                        .round();
                break;
        }

        // init the list view and its adapter
        ListContainer list = (ListContainer) findComponentById(ResourceTable.Id_list);
        list.setItemProvider(new SampleItemProvider());
    }

    private class SampleItemProvider extends BaseItemProvider {

        @Override
        public int getCount() {
            return mDataList.size();
        }

        @Override
        public ListData getItem(int position) {
            return mDataList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Component getComponent(int position, Component convertView, ComponentContainer componentContainer) {
            final ViewHolder holder;
            if (convertView == null) {
                convertView = LayoutScatter.getInstance(ListAbility.this).parse(ResourceTable.Layout_layout_list_item, null, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ListData item = getItem(position);

            // provide support for selected state
            updateCheckedState(holder, item);
            holder.imageView.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    // when the image is clicked, update the selected state
                    ListData data = getItem(position);
                    data.setChecked(!data.isChecked);
                    updateCheckedState(holder, data);
                }
            });
            holder.textView.setText(item.data);

            return convertView;
        }


        private void updateCheckedState(ViewHolder holder, ListData item) {
            if (item.isChecked) {
                TextDrawable drawable = mDrawableBuilder.build("", 0xffcdcbcb);
                holder.imageView.addDrawTask(new Component.DrawTask() {
                    @Override
                    public void onDraw(Component component, Canvas canvas) {
                        drawable.setBounds(new Rect(0, 0, holder.imageView.getWidth(), holder.imageView.getHeight()));
                        drawable.drawToCanvas(canvas);
                    }
                });
                ShapeElement shapeElement = new ShapeElement();
                shapeElement.setRgbColor(RgbColor.fromArgbInt(HIGHLIGHT_COLOR));
                holder.view.setBackground(shapeElement);
                holder.checkIcon.setVisibility(Component.VISIBLE);
            }
            else {
                TextDrawable drawable = mDrawableBuilder.build(String.valueOf(item.data.charAt(0)), mColorGenerator.getColor(item.data));
                // holder.imageView.setImageDrawable(drawable);
                holder.imageView.addDrawTask(new Component.DrawTask() {
                    @Override
                    public void onDraw(Component component, Canvas canvas) {
                        drawable.setBounds(new Rect(0, 0, holder.imageView.getWidth(), holder.imageView.getHeight()));
                        drawable.drawToCanvas(canvas);
                    }
                });
                ShapeElement shapeElement = new ShapeElement();
                shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.TRANSPARENT.getValue()));
                holder.view.setBackground(shapeElement);
                holder.checkIcon.setVisibility(Component.HIDE);
            }
        }
    }

    private static class ViewHolder {

        private Component view;

        private Image imageView;

        private Text textView;

        private Image checkIcon;

        private ViewHolder(Component view) {
            this.view = view;
            imageView = (Image) view.findComponentById(ResourceTable.Id_imageView);
            textView = (Text) view.findComponentById(ResourceTable.Id_textView);
            checkIcon = (Image) view.findComponentById(ResourceTable.Id_check_icon);
        }
    }

    private static class ListData {

        private String data;

        private boolean isChecked;

        public ListData(String data) {
            this.data = data;
        }

        public void setChecked(boolean isChecked) {
            this.isChecked = isChecked;
        }
    }
}
