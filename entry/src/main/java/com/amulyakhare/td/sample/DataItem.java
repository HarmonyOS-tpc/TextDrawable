package com.amulyakhare.td.sample;

import ohos.agp.components.element.Element;

/**
 * @author amulya
 * @datetime 17 Oct 2014, 3:50 PM
 */
public class DataItem {
    private final String label;

    private final Element drawable;

    private final int navigationInfo;

    public DataItem(String label, Element drawable, int navigationInfo) {
        this.label = label;
        this.drawable = drawable;
        this.navigationInfo = navigationInfo;
    }

    public String getLabel() {
        return label;
    }

    public Element getDrawable() {
        return drawable;
    }

    public int getNavigationInfo() {
        return navigationInfo;
    }
}
