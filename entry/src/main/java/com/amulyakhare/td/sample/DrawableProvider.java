package com.amulyakhare.td.sample;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.FrameAnimationElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * @author amulya
 * @datetime 17 Oct 2014, 4:02 PM
 */
public class DrawableProvider {
    public static final int SAMPLE_RECT = 1;
    public static final int SAMPLE_ROUND_RECT = 2;
    public static final int SAMPLE_ROUND = 3;
    public static final int SAMPLE_RECT_BORDER = 4;
    public static final int SAMPLE_ROUND_RECT_BORDER = 5;
    public static final int SAMPLE_ROUND_BORDER = 6;
    public static final int SAMPLE_MULTIPLE_LETTERS = 7;
    public static final int SAMPLE_FONT = 8;
    public static final int SAMPLE_SIZE = 9;
    public static final int SAMPLE_ANIMATION = 10;
    public static final int SAMPLE_MISC = 11;

    private final ColorGenerator mGenerator;
    private final Context mContext;

    public DrawableProvider(Context context) {
        mGenerator = ColorGenerator.DEFAULT;
        mContext = context;
    }

    public TextDrawable getRect(String text) {
        return TextDrawable.builder()
                .buildRect(text, mGenerator.getColor(text));
    }

    public TextDrawable getRound(String text) {
        return TextDrawable.builder()
                .buildRound(text, mGenerator.getColor(text));
    }

    public TextDrawable getRoundRect(String text) {
        return TextDrawable.builder()
                .buildRoundRect(text, mGenerator.getColor(text), toPx(10));
    }

    public TextDrawable getRectWithBorder(String text) {
        return TextDrawable.builder()
                .beginConfig()
                    .withBorder(toPx(2))
                .endConfig()
                .buildRect(text, mGenerator.getColor(text));
    }

    public TextDrawable getRoundWithBorder(String text) {
        return TextDrawable.builder()
                .beginConfig()
                    .withBorder(toPx(2))
                .endConfig()
                .buildRound(text, mGenerator.getColor(text));
    }

    public TextDrawable getRoundRectWithBorder(String text) {
        return TextDrawable.builder()
                .beginConfig()
                    .withBorder(toPx(2))
                .endConfig()
                .buildRoundRect(text, mGenerator.getColor(text), toPx(10));
    }

    public TextDrawable getRectWithMultiLetter() {
        String text = "ak";
        return TextDrawable.builder()
                .beginConfig()
                    .fontSize(toPx(10))
                    .toUpperCase()
                .endConfig()
                .buildRect(text, mGenerator.getColor(text));
    }

    public TextDrawable getRoundWithCustomFont() {
        String text = "Bold";
        return TextDrawable.builder()
                .beginConfig()
                    .useFont(Font.SERIF)
                    .fontSize(toPx(15))
                    .textColor(0xfff58559)
                    .bold()
                .endConfig()
                .buildRect(text, Color.DKGRAY.getValue());
    }

    public Element getRectWithCustomSize() {
        String leftText = "I";
        String rightText = "J";

        TextDrawable.IBuilder builder = TextDrawable.builder()
                .beginConfig()
                    .width(toPx(29))
                    .withBorder(toPx(2))
                .endConfig()
                .rect();

        TextDrawable left = builder
                .build(leftText, mGenerator.getColor(leftText));

        TextDrawable right = builder
                .build(rightText, mGenerator.getColor(rightText));

        StateElement se = new StateElement();
        se.addState(null, left);
        se.addState(null, right);
        return se;
    }

    public Element getRectWithAnimation() {
        TextDrawable.IBuilder builder = TextDrawable.builder()
                .rect();

        FrameAnimationElement animationDrawable = new FrameAnimationElement();
        for (int i = 5; i > 0; i--) {
            TextDrawable frame = builder.build(String.valueOf(i), mGenerator.getRandomColor());
            animationDrawable.addFrame(frame, 1200);
        }
        animationDrawable.setOneShot(false);
        animationDrawable.start();

        return animationDrawable;
    }

    public int toPx(int dp) {
        return AttrHelper.vp2px(dp, mContext);
    }
}
