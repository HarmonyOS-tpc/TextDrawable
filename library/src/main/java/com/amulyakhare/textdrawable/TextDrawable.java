package com.amulyakhare.textdrawable;

import java.util.Locale;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.TextAlignment;

/**
 * @author amulya
 * @datetime 14 Oct 2014, 3:53 PM
 */
public class TextDrawable extends ShapeElement {
    private static final float SHADE_FACTOR = 0.9f;

    private final Paint textPaint;

    private final String text;

    private final int color;

    private final int shape;

    private final int height;

    private final int width;

    private final int fontSize;

    private final float radius;

    private final int borderThickness;

    private TextDrawable(Builder builder) {
        super();

        // shape properties
        shape = builder.shape;
        height = builder.height;
        width = builder.width;
        radius = builder.radius;

        // text and drawable color
        text = builder.toUpperCase ? builder.text.toUpperCase(Locale.ROOT) : builder.text;
        color = builder.color;

        // text paint settings
        fontSize = builder.fontSize;
        textPaint = new Paint();
        textPaint.setColor(new Color(builder.textColor));
        textPaint.setAntiAlias(true);
        textPaint.setFakeBoldText(builder.isBold);
        textPaint.setStyle(Paint.Style.FILL_STYLE);
        textPaint.setFont(builder.font);
        textPaint.setTextAlign(TextAlignment.CENTER);
        textPaint.setStrokeWidth(builder.borderThickness);

        // border settings
        borderThickness = builder.borderThickness;
    }

    private int getDarkerShade(int color) {
        RgbColor rgb = RgbColor.fromArgbInt(color);
        return Color.rgb((int) (SHADE_FACTOR * rgb.getRed()),
                (int) (SHADE_FACTOR * rgb.getGreen()),
                (int) (SHADE_FACTOR * rgb.getBlue()));
    }

    @Override
    public void drawToCanvas(Canvas canvas) {
        setShape(shape);
        setCornerRadius(radius);
        setStroke(borderThickness, RgbColor.fromArgbInt(getDarkerShade(color)));
        setRgbColor(RgbColor.fromArgbInt(color));
        super.drawToCanvas(canvas);

        // draw text
        Rect rect = getBounds();
        int tempWidth = this.width < 0 ? rect.getWidth() : this.width;
        int tempHeight = this.height < 0 ? rect.getHeight() : this.height;
        int tempFontSize = this.fontSize < 0 ? (Math.min(tempWidth, tempHeight) / 2) : this.fontSize;
        textPaint.setTextSize(tempFontSize);
        canvas.drawText(textPaint, text, rect.getCenterX(),
                rect.getCenterY() - ((textPaint.descent() + textPaint.ascent()) / 2));
    }

    @Override
    public void setAlpha(int alpha) {
        textPaint.setAlpha(alpha);
    }

    public static IShapeBuilder builder() {
        return new Builder();
    }

    public static class Builder implements IConfigBuilder, IShapeBuilder, IBuilder {
        private String text;

        private int color;

        private int borderThickness;

        private int width;

        private int height;

        private Font font;

        private int shape;

        private int textColor;

        private int fontSize;

        private boolean isBold;

        private boolean toUpperCase;

        private float radius;

        private Builder() {
            text = "";
            color = Color.GRAY.getValue();
            textColor = Color.WHITE.getValue();
            borderThickness = 0;
            width = -1;
            height = -1;
            shape = ShapeElement.RECTANGLE;
            font = Font.SANS_SERIF;
            fontSize = -1;
            isBold = false;
            toUpperCase = false;
        }

        public IConfigBuilder width(int width) {
            this.width = width;
            return this;
        }

        public IConfigBuilder height(int height) {
            this.height = height;
            return this;
        }

        public IConfigBuilder textColor(int color) {
            this.textColor = color;
            return this;
        }

        public IConfigBuilder withBorder(int thickness) {
            this.borderThickness = thickness;
            return this;
        }

        public IConfigBuilder useFont(Font font) {
            this.font = font;
            return this;
        }

        public IConfigBuilder fontSize(int size) {
            this.fontSize = size;
            return this;
        }

        public IConfigBuilder bold() {
            this.isBold = true;
            return this;
        }

        public IConfigBuilder toUpperCase() {
            this.toUpperCase = true;
            return this;
        }

        @Override
        public IConfigBuilder beginConfig() {
            return this;
        }

        @Override
        public IShapeBuilder endConfig() {
            return this;
        }

        @Override
        public IBuilder rect() {
            this.shape = ShapeElement.RECTANGLE;
            return this;
        }

        @Override
        public IBuilder round() {
            this.shape = ShapeElement.OVAL;
            return this;
        }

        @Override
        public IBuilder roundRect(int radius) {
            this.radius = radius;
            this.shape = ShapeElement.RECTANGLE;

            return this;
        }

        @Override
        public TextDrawable buildRect(String text, int color) {
            rect();
            return build(text, color);
        }

        @Override
        public TextDrawable buildRoundRect(String text, int color, int radius) {
            roundRect(radius);
            return build(text, color);
        }

        @Override
        public TextDrawable buildRound(String text, int color) {
            round();
            return build(text, color);
        }

        @Override
        public TextDrawable build(String text, int color) {
            this.color = color;
            this.text = text;
            return new TextDrawable(this);
        }
    }

    public interface IConfigBuilder {
        IConfigBuilder width(int width);

        IConfigBuilder height(int height);

        IConfigBuilder textColor(int color);

        IConfigBuilder withBorder(int thickness);

        IConfigBuilder useFont(Font font);

        IConfigBuilder fontSize(int size);

        IConfigBuilder bold();

        IConfigBuilder toUpperCase();

        IShapeBuilder endConfig();
    }

    public interface IBuilder {
        TextDrawable build(String text, int color);
    }

    public interface IShapeBuilder {
        IConfigBuilder beginConfig();

        IBuilder rect();

        IBuilder round();

        IBuilder roundRect(int radius);

        TextDrawable buildRect(String text, int color);

        TextDrawable buildRoundRect(String text, int color, int radius);

        TextDrawable buildRound(String text, int color);
    }
}
