package com.amulyakhare.textdrawable.util;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;

/**
 * @author amulya
 * @datetime 14 Oct 2014, 5:20 PM
 */
public class ColorGenerator {
    public static ColorGenerator DEFAULT;

    public static ColorGenerator MATERIAL;

    static {
        DEFAULT = create(Arrays.asList(
                0xfff16364,
                0xfff58559,
                0xfff9a43e,
                0xffe4c62e,
                0xff67bf74,
                0xff59a2be,
                0xff2093cd,
                0xffad62a7,
                0xff805781
        ));
        MATERIAL = create(Arrays.asList(
                0xffe57373,
                0xfff06292,
                0xffba68c8,
                0xff9575cd,
                0xff7986cb,
                0xff64b5f6,
                0xff4fc3f7,
                0xff4dd0e1,
                0xff4db6ac,
                0xff81c784,
                0xffaed581,
                0xffff8a65,
                0xffd4e157,
                0xffffd54f,
                0xffffb74d,
                0xffa1887f,
                0xff90a4ae
        ));
    }

    private final List<Integer> mColors;
    private final SecureRandom mRandom;

    public static ColorGenerator create(List<Integer> colorList) {
        return new ColorGenerator(colorList);
    }

    private ColorGenerator(List<Integer> colorList) {
        mColors = colorList;
        mRandom = new SecureRandom(long2bytes(System.currentTimeMillis()));
    }

    private byte[] long2bytes(long lon) {
        byte[] result = new byte[8];
        result[0] = (byte)((lon >> 56) & 0xFF);
        result[1] = (byte)((lon >> 48) & 0xFF);
        result[2] = (byte)((lon >> 40) & 0xFF);
        result[3] = (byte)((lon >> 32) & 0xFF);
        result[4] = (byte)((lon >> 24) & 0xFF);
        result[5] = (byte)((lon >> 16) & 0xFF);
        result[6] = (byte)((lon >> 8) & 0xFF);
        result[7] = (byte)(lon & 0xFF);
        return result;
    }

    public int getRandomColor() {
        return mColors.get(mRandom.nextInt(mColors.size()));
    }

    public int getColor(Object key) {
        return mColors.get(Math.abs(key.hashCode() % mColors.size()));
    }
}
